
# Box domotique 

Boitier 140x140x60.5 mm acceptant un Raspberry pi 3 ou 4. Installation de Home Assistant et montage dans cette video: https://youtu.be/e7MKEdTGX1U 





## Composants
Liens:
- [Raspberry Pi 4 (Amazon)](https://amzn.to/3WLR8i7)
- [Raspberry Pi 4 (Aliexpress)](https://s.click.aliexpress.com/e/_DBRj51N)
- [Aimants](https://amzn.to/3C1js8k)
- [Insert M2.5 x 5mm](https://fr.aliexpress.com/item/1005003582355741.html?spm=a2g0o.productlist.main.1.62125614E3Fo89&algo_pvid=264e0c8c-611d-4ccb-85e1-948d94acffaa&algo_exp_id=264e0c8c-611d-4ccb-85e1-948d94acffaa-0&pdp_ext_f=%7B%22sku_id%22%3A%2212000026370649789%22%7D&pdp_npi=2%40dis%21EUR%213.93%211.14%21%21%21%21%21%40211bd3cb16713578460683489d0774%2112000026370649789%21sea&curPageLogUid=oCgPL8Ynnqny)
- [Vis en laiton M2.5 x 5mm](https://fr.aliexpress.com/item/4000156733513.html?spm=a2g0o.productlist.main.3.256e136dVy2sIh&algo_pvid=33091f31-7ae8-44b2-8616-87ecf285900e&algo_exp_id=33091f31-7ae8-44b2-8616-87ecf285900e-1&pdp_ext_f=%7B%22sku_id%22%3A%2212000026943542810%22%7D&pdp_npi=2%40dis%21EUR%212.05%211.74%21%21%21%21%21%40211bc2a016713577763345510d073a%2112000026943542810%21sea&curPageLogUid=6ikSjFsVmvBM)
Optionnels
- [Ventilo Noctua 5v](https://amzn.to/3sB9Qwk)
- [Bandeau WS2812b (144/m)](https://fr.aliexpress.com/item/2036819167.html?UTABTest=aliabtest359606_508211&aff_fcid=f70ffdaeb0074f95bbbd118f6b4938fb-1671880749238-07460-_DdZ5urd&tt=CPS_NORMAL&aff_fsk=_DdZ5urd&aff_platform=shareComponent-detail&sk=_DdZ5urd&aff_trace_key=f70ffdaeb0074f95bbbd118f6b4938fb-1671880749238-07460-_DdZ5urd&terminal_id=fd3a99d8b9ca4abfa866a70786b7e17e&OLP=1084900408_f_group0&o_s_id=1084900408&afSmartRedirect=y)
- [D1 mini](https://s.click.aliexpress.com/e/_DF7rKex)
- [SSD](https://amzn.to/3Lfx0yQ)
- [Adaptateur SSD/USB](https://amzn.to/3Ppu06c)

Il existe plein d'alternatives au Raspberry, en ce moment le "wyse 5060" est à la mode (notamment sur Ebay)


## Connections 
- SSD sur Rasberry
```bash
  -> Port USB du raspberry pi
```
- Ventilo sur Raspberry
```bash
  Rouge -> 5V (broche 2) 
  Noir -> GND (broche 6)
```
- D1 mini sur Raspberry

```bash
  5V -> 5V (broche 2) 
  GND -> GND (broche 6)
```

- WS2812b x 4 (en parallèle) sur D1 mini

```bash
  5V -> 5V
  GND -> GND
  DATA -> D1 (GPIO5)
```
## Filaments

| Couleur            | Hex                                                               | Filament                                                           |
| ----------------- | ------------------------------------------------------------------ | ------------------------------------------------------------------ |
| Boitier| ![#ffffff](https://via.placeholder.com/10/ffffff?text=+)| [Marbre](https://www.wanhaofrance.com/products/pla-premium-marbre-1kg-1-75-ender3-creality-wanhao?ref=abrege) 
| Face avant | ![#0a192f](https://via.placeholder.com/10/0a192f?text=+) | [Noir mat](https://amzn.to/3jr6tGO)
| Diffuseur | ![#ffffff](https://via.placeholder.com/10/ffffff?text=+) | [Blanc](https://www.filimprimante3d.fr/filament-pla-175-mm/3245-filament-premium-pla-175mm-blanc-arctique-1kg-spectrum.html)



## Auteurs

- [@tsoriano_geneva](https://gitlab.com/tsoriano_geneva)

